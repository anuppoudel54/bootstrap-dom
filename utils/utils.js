// function to create elements
export function createElements() {
  for (let i = 0; i < arguments.length; i++) {
    return document.createElement(arguments[i]);
  }
}

// function to append child to parent where first parametar is parent followed by children
export function appendChildFunc() {
  for (let i = 1; i < arguments.length; i++) {
    arguments[0].appendChild(arguments[i]);
  }
}
// creates elements with attributes
export function createElementWithAtrributes(obj) {
  const element = createElements(obj.element);
  try {
    Object.keys(obj)
      .filter((key) => key !== "element")
      .map((key) => {
        element.setAttribute(key, obj[key]);
      });
  } catch (error) {
    console.log(error);
  }

  return element;
}
