import nav from "./nav.js";
const loadContent = () => {
  const contentDiv = document.getElementById("app");
  contentDiv.appendChild(nav);
  // contentDiv.innerText = location.hash;
};
window.addEventListener("hashchange", loadContent);
if (!location.hash) {
  location.hash = "#home";
}
console.log(nav);
loadContent();
