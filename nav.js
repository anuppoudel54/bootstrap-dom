import { createElementWithAtrributes, appendChildFunc } from "./utils/utils.js";

const mainNavObj = {
  "element": "nav",
  "id": "nav",
  "class": "navbar navbar-expand-lg navbar-light bg-light",
};
const navigationMenuItemsContainerObj = {
  "element": "div",
  "id": "navbarSupportedContent",
  "class": "collapse navbar-collapse",
};

//Navigation menu items
const navItems = {
  "home": { "title": "Home", "url": "#home" },
  "about": { "title": "About", "url": "#about" },
  "contact": { "title": "Contact", "url": "#contact" },
};
const logoItem = {
  "title": "Asha",
  "url": "#home",
};
const logoClass = {
  "element": "a",
  "class": "navbar-brand",
};
const menuItemClass = {
  "element": "a",
  "class": "nav-link",
};
const navBarUnorderedListObj = {
  "element": "ul",
  "class": "navbar-nav mr-auto",
};
const navBarUnorderedListItems = {
  "element": "li",
  "class": "nav-item",
};
const hamburgerButtonClass = {
  "element": "button",
  "class": "navbar-toggler",
  "type": "button",
  "data-toggle": "collapse",
  "data-target": "#navbarSupportedContent",
  "aria-controls": "navbarSupportedContent",
  "aria-expanded": "false",
  "aria-label": "Toggle navigation",
};
const hamburgerSpanClass = {
  "element": "span",
  "class": "navbar-toggler-icon",
};

// // creates navigation menu and items
function createNavMenu() {
  const navBar = createElementWithAtrributes(mainNavObj);
  const logo = createElementWithAtrributes(logoClass);
  logo.setAttribute("href", logoItem.title);
  logo.innerText = logoItem.title;

  const hamburgerButton = createElementWithAtrributes(hamburgerButtonClass);
  const hamburgerSpan = createElementWithAtrributes(hamburgerSpanClass);
  hamburgerButton.appendChild(hamburgerSpan);
  appendChildFunc(navBar, logo, hamburgerButton);
  const navigationMenuItemsContainer = createElementWithAtrributes(
    navigationMenuItemsContainerObj
  );
  const navBarUnorderedList = createElementWithAtrributes(
    navBarUnorderedListObj
  );
  for (let key in navItems) {
    const navBarUnorderedListItem = createElementWithAtrributes(
      navBarUnorderedListItems
    );
    const menuItem = createElementWithAtrributes(menuItemClass);
    menuItem.setAttribute("href", navItems[key].url);
    menuItem.innerText = navItems[key].title;
    appendChildFunc(navBarUnorderedListItem, menuItem);
    appendChildFunc(navBarUnorderedList, navBarUnorderedListItem);
    appendChildFunc(navigationMenuItemsContainer, navBarUnorderedList);
  }
  appendChildFunc(navBar, navigationMenuItemsContainer);
  return navBar;
}

const nav = createNavMenu();
export default nav;
